FROM gitlab.scsuk.net:5005/scs-systems/ext_registry/centos:7.6.1810
RUN yum -y install httpd perl && \
    rm -f /etc/httpd/conf.d/welcome.conf
COPY webfiles/scs_http.conf /etc/httpd/conf.d/
CMD /usr/sbin/httpd -DFOREGROUND
