#!/bin/bash
set -e
TAG="4.0"
IMAGE="gitlab.scsuk.net:5005/scs-systems/phase-4/scs-httpd:$TAG"
set -x
docker build --no-cache -t "$IMAGE" .
docker push "$IMAGE"
set +x

